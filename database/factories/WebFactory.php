<?php

namespace Database\Factories;

use App\Models\Monitoring;
use App\Models\Web;
use Illuminate\Database\Eloquent\Factories\Factory;

class WebFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Web::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => function(){
                return Monitoring::factory()->create()->id;
            },
            'url' => $this->faker->url
        ];
    }
}
