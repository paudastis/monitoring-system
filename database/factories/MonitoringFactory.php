<?php

namespace Database\Factories;

use App\Models\Monitoring;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class MonitoringFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Monitoring::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => function(){
                return User::factory()->create()->id;
            },
            'is_active' => $this->faker->boolean,
            'type' => 'Web',
            'created_at' => $this->faker->time(),
            'updated_at' => $this->faker->time()
        ];
    }
}
