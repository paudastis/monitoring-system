<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToHtmlViewTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('html_view', function(Blueprint $table)
		{
			$table->foreign('monitoring_id', 'fk_html_web')->references('id')->on('web')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('html_view', function(Blueprint $table)
		{
			$table->dropForeign('fk_html_web');
		});
	}

}
