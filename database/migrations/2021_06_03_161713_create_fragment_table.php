<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFragmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fragment', function(Blueprint $table)
		{
			$table->integer('monitoring_id')->primary();
			$table->text('fragment');
			$table->string('operator', 2)->nullable();
			$table->string('value_to_compare')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fragment');
	}

}
