<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToJsonResponseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('json_response', function(Blueprint $table)
		{
			$table->foreign('monitoring_id', 'fk_json_api')->references('id')->on('api')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('json_response', function(Blueprint $table)
		{
			$table->dropForeign('fk_json_api');
		});
	}

}
