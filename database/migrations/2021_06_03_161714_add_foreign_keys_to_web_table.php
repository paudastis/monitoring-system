<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToWebTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('web', function(Blueprint $table)
		{
			$table->foreign('id', 'fk_web_monitoring')->references('id')->on('monitoring')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('web', function(Blueprint $table)
		{
			$table->dropForeign('fk_web_monitoring');
		});
	}

}
