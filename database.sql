CREATE TABLE web (
    id int(255) NOT NULL,
    url text NOT NULL,
    CONSTRAINT pk_web PRIMARY KEY (id),
    CONSTRAINT fk_web_monitoring FOREIGN KEY (id) REFERENCES monitoring (id)
) ENGINE=InnoDB;

CREATE TABLE html_view(
    path VARCHAR(255) NOT NULL,
    monitoring_id int(255) NOT NULL,
    created_at datetime NOT NULL,
    CONSTRAINT pk_view PRIMARY KEY (path),
    CONSTRAINT fk_html_web FOREIGN KEY (monitoring_id) REFERENCES web (id)
) ENGINE=InnoDB;

CREATE TABLE json_response(
  path VARCHAR(255) NOT NULL,
  monitoring_id int(255) NOT NULL,
  created_at datetime NOT NULL,
  CONSTRAINT pk_response PRIMARY KEY (path),
  CONSTRAINT fk_json_api FOREIGN KEY (monitoring_id) REFERENCES api (id)
) ENGINE=InnoDB;

CREATE TABLE fragment(
  monitoring_id int(255) NOT NULL,
  fragment text NOT NULL,
  operator VARCHAR(2),
  value_to_compare VARCHAR(255),
  CONSTRAINT pk_fragment PRIMARY KEY (monitoring_id),
  CONSTRAINT fk_fragment_monitoring FOREIGN KEY (monitoring_id) REFERENCES monitoring (id)
) ENGINE=InnoDB;
