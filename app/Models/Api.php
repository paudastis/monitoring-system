<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    use HasFactory;
    protected $table = 'api';
    public $timestamps = false;

    public function monitoring(){
        return $this->belongsTo(Monitoring::class, 'id');
    }

    public function files(){
        return $this->hasMany(JsonResponse::class, 'monitoring_id')->orderBy('created_at', 'desc');
    }
}
