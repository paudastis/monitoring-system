<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Monitoring extends Model
{
    use HasFactory;
    protected $table = 'monitoring';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function fragment(){
        return $this->belongsTo(Fragment::class, 'id', 'monitoring_id');
    }

}
