<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JsonResponse extends Model
{
    use HasFactory;
    protected $table = 'json_response';
    public $timestamps = false;

    public function api(){
        return $this->belongsTo(Api::class, 'monitoring_id');
    }
}
