<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HtmlView extends Model
{
    use HasFactory;
    protected $table = 'html_view';
    public $timestamps = false;

    public function web(){
        return $this->belongsTo(Web::class, 'monitoring_id');
    }
}
