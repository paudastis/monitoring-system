<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fragment extends Model
{
    use HasFactory;
    protected $table = 'fragment';
    protected $primaryKey = 'monitoring_id';
    public $timestamps = false;

    public function monitoring(){
        return $this->belongsTo(Monitoring::class, 'monitoring_id');
    }
}
