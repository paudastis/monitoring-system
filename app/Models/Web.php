<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Web extends Model
{
    use HasFactory;
    protected $table = 'web';
    public $timestamps = false;

    public function monitoring(){
        return $this->belongsTo(Monitoring::class, 'id');
    }

    public function files(){
        return $this->hasMany(HtmlView::class, 'monitoring_id')->orderBy('created_at', 'desc');
    }

}
