<?php

namespace App\Console\Commands;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\SendEmailController;
use App\Http\Controllers\WebController;
use App\Models\Api;
use App\Models\HtmlView;
use App\Models\JsonResponse;
use App\Models\Monitoring;
use App\Models\Web;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use function Symfony\Component\String\s;

class MonitoringCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitoring:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks the status of all active monitorings';

    /**
     * Execute the console command.
     *
     * @return int
     */

    private function api_call($method, $uri, $header, $data){
        $log_error = '[Check-Error] Api call fail.';
        if($header){
            try {
                return Http::withHeaders(json_decode($header, true))->$method($uri, json_decode($data, true));
            } catch (\HttpException $e) {
                Log::error($log_error);
            } catch (\Exception $e) {
                Log::error($log_error);
            }
        }else {
            try {
                return Http::$method($uri, json_decode($data, true));
            } catch (\HttpException $e) {
                Log::error($log_error);
            } catch (\Exception $e) {
                Log::error($log_error);
            }
        }
        return null;
    }

    private function api_save_response($api, $response){
        Log::info('[Check] Saving new Api Response in monitoring ' . $api->id);
        $file_name = $api->id.time().'.json';
        Storage::disk('json')->put($file_name, $response);
        $json_response = new JsonResponse();
        $json_response->path = $file_name;
        $json_response->monitoring_id = $api->id;
        $json_response->created_at = date('Y-m-d H:i:s');
        $json_response->save();
    }

    private function api_check_fragment($actual_response, $old_response, $fragment){
        $actual_value = ApiController::get_fragment_value($fragment->fragment, $actual_response);
        $old_response = json_decode($old_response, true);
        $old_value = ApiController::get_fragment_value($fragment->fragment, $old_response);
        if ($actual_value != $old_value){
            if($fragment->operator){
                if($this->api_check_fragment_condition($fragment, $actual_value)){
                    return true;
                }
            }else{
                return true;
            }
        }
        return false;
    }

    private function api_check_fragment_condition($fragment, $actual_value){
        $result = false;
        switch($fragment->operator){
            case '==':
                if ($actual_value == $fragment->value_to_compare){
                    $result = true;
                }
                break;
            case '!=':
                if ($actual_value != $fragment->value_to_compare){
                    $result = true;
                }
                break;
            case '<':
                if ($actual_value < $fragment->value_to_compare){
                    $result = true;
                }
                break;
            case '<=':
                if ($actual_value <= $fragment->value_to_compare){
                    $result = true;
                }
                break;
            case '>':
                if ($actual_value > $fragment->value_to_compare){
                    $result = true;
                }
                break;
            case '>=':
                if ($actual_value >= $fragment->value_to_compare){
                    $result = true;
                }
                break;
            default:
                $result = false;
        }
        return $result;
    }

    private function api_check($api, $fragment){
        $actual_response = $this->api_call($api->method, $api->uri, $api->header, $api->data);
        $last_file = JsonResponse::where('monitoring_id', $api->id)->orderBy('created_at', 'desc')->first();
        if($actual_response && Storage::disk('json')->exists($last_file->path)){
            $old_response = Storage::disk('json')->get($last_file->path);
            $save = false;
            if($fragment){
                $save = $this->api_check_fragment($actual_response, $old_response, $fragment);
            } else {
                $save = $actual_response != $old_response;
            }
            if($save) {
                $this->api_save_response($api, $actual_response);
                if($api->monitoring->user->notification) {
                    SendEmailController::notification_monitoring($api->id, $api->monitoring->user->email);
                }
            }
        }
    }

    private function array_equals($array_1, $array_2){
        if (count($array_1) == count($array_2)){
            for($i = 0; $i < count($array_1); $i++){
                if($array_1[$i] != $array_2[$i]) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    private function web_check_fragment_condition($fragment, $elements){
        foreach ($elements as $element){
            if($this->api_check_fragment_condition($fragment, $element)) {
                return true;
            }
        }
        return false;
    }

    private function web_check_fragment($actual_html, $old_html, $fragment){
        $filters = unserialize($fragment->fragment);
        $old_elements = WebController::apply_filters_html($old_html, $filters[0], $filters[1], $filters[2], $filters[3]);
        $actual_elements = WebController::apply_filters_html($actual_html, $filters[0], $filters[1], $filters[2], $filters[3]);
        if($filters[4]){
            $old_elements = WebController::get_content_of_elements($old_elements);
            $actual_elements = WebController::get_content_of_elements($actual_elements);
        }
        if(!$this->array_equals($old_elements, $actual_elements)){
            if($fragment->operator){
                if($this->web_check_fragment_condition($fragment, $actual_elements)) {
                    return true;
                }
            }else{
                return true;
            }
        }
        return false;
    }

    private function web_save_html($web, $html){
        Log::info('[Check] Saving new Web Response in monitoring ' . $web->id);
        $file_name = $web->id.time().'.html';
        Storage::disk('html')->put($file_name, $html);
        $html_view = new HtmlView();
        $html_view->path = $file_name;
        $html_view->monitoring_id = $web->id;
        $html_view->created_at = date('Y-m-d H:i:s');
        $html_view->save();
    }

    private function web_check($web, $fragment){
        $lastView = HtmlView::where('monitoring_id', $web->id)->orderBy('created_at', 'desc')->first();
        if(Storage::disk('html')->exists($lastView->path)) {
            $old_html = Storage::disk('html')->get($lastView->path);
            $actual_html = WebController::url2String($web->url);
            $save = false;
            if($fragment){
                 $save = $this->web_check_fragment($actual_html, $old_html, $fragment);
            } else {
                $save = $old_html != $actual_html;
            }
            if($save) {
                $this->web_save_html($web, $actual_html);
                if($web->monitoring->user->notification){
                    SendEmailController::notification_monitoring($web->id, $web->monitoring->user->email);
                }
            }
        }
    }

    public function handle()
    {
        Log::info('[Check-Start] Monitoring checks start.');
        $monitorings = Monitoring::where('is_active', true)->get();
        foreach ($monitorings as $monitoring) {
            Log::info('[Check] Monitoring ' . $monitoring->id . ' check start.');
            $fragment = null;
            if(isset($monitoring->fragment)){
                $fragment = $monitoring->fragment;
            }
            $web = Web::find($monitoring->id);
            $api = Api::find($monitoring->id);
            if($api) {
                $this->api_check($api, $fragment);

            } elseif ($web){
                $this->web_check($web, $fragment);
            }
            $monitoring->touch();
        }
        Log::info('[Check-End] Monitoring checks end.');
        return 0;
    }
}
