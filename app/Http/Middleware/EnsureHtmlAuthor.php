<?php

namespace App\Http\Middleware;

use App\Models\HtmlView;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EnsureHtmlAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $html = HtmlView::where('path', $request->route('path').'.html')->first();
        if ($html && \Auth::user()->id == $html->web->monitoring->user->id && Storage::disk('html')->exists($html->path)) {
            return $next($request);
        }
        return redirect('/dashboard');
    }
}
