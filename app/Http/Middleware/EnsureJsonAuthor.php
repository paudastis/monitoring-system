<?php

namespace App\Http\Middleware;

use App\Models\JsonResponse;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EnsureJsonAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $json = JsonResponse::where('path', $request->route('path').'.json')->first();
        if ($json && \Auth::user()->id == $json->api->monitoring->user->id && Storage::disk('json')->exists($json->path)) {
            return $next($request);
        }
        return redirect('/dashboard');
    }
}
