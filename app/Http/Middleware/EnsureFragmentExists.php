<?php

namespace App\Http\Middleware;

use App\Models\Fragment;
use Closure;
use Illuminate\Http\Request;

class EnsureFragmentExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $fragment = Fragment::find($request->route('id'));
        if ($fragment) {
            return $next($request);
        }
        return redirect('/dashboard')->with([
            'message-type' => 'error',
            'message-content'=>'This monitoring is not monitoring any fragments.'
        ]);
    }
}
