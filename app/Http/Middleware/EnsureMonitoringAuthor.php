<?php

namespace App\Http\Middleware;

use App\Models\Monitoring;
use Closure;
use Illuminate\Http\Request;

class EnsureMonitoringAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $monitoring = Monitoring::find($request->route('id'));
        if ($monitoring && \Auth::user()->id == $monitoring->user_id) {
            return $next($request);
        }
        return redirect('/dashboard')->with([
            'message-type' => 'error',
            'message-content'=>'You are not the author of this monitoring'
        ]);
    }
}
