<?php

namespace App\Http\Controllers;

use App\Models\Fragment;
use App\Models\HtmlView;
use App\Models\Monitoring;
use App\Models\Web;
use DOMDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Masterminds\HTML5;

class WebController extends Controller
{
    public function create(){
        return view('web.create');
    }

    public static function url2String($url){
        $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL,$url);
        return curl_exec($ch);
    }

    public function getHtml(Request $request){
        $html = self::url2String(urldecode($request->input('url')));
        return response()->json([
           'html'=> $html
        ]);
    }

    public function preview_get_elements($id, Request $request){
        $tag = $request->input('tag');
        $element_id = $request->input('id');
        $class = $request->input('class');
        $other = $request->input('other');
        return response()->json([
            'elements'=>self::apply_filters($id, $tag, $element_id, $class, $other)
        ]);
    }

    public static function apply_filters_html($html, $tag, $element_id, $class, $other){
        $response = self::get_elements_by_tag($html, $tag);
        if($element_id){
            $response = self::get_elements_by_id($response, $element_id);
        }
        if($class){
            $response = self::get_elements_by_class($response, $class);
        }
        if($other){
            $response = self::new_element_filter($response, $other);
        }
        return $response;
    }

    public static function apply_filters($id, $tag, $element_id, $class, $other){
        $html_view = HtmlView::where('monitoring_id', $id)->orderBy('created_at', 'desc')->first();
        $html = Storage::disk('html')->get($html_view->path);
        return self::apply_filters_html($html, $tag, $element_id, $class, $other);
    }

    public function save(Request $request){
        //Validate data
        $request->validate([
            'url'=>'required',
        ]);
        //Do the website call to ensure and get the response
        $url = $request->input('url');
        $html = $this->url2String($url);
        if(!$html){
            return redirect()->route('web.create')->with([
                'message-type' => 'error',
                'message-content'=>'The web monitoring has not been created'
            ]);
        }
        //Save Monitoring
        $user = \Auth::user();
        $monitoring = new Monitoring();
        $monitoring->user_id = $user->id;
        $monitoring->is_active = true;
        $monitoring->type = 'Web';
        $monitoring->save();
        //Save Web
        $web = new Web();
        $web->id = $monitoring->id;
        $web->url = $url;
        $web->save();
        //Storage HTML
        $file_name = $user->id.$monitoring->id.time().'.html';
        Storage::disk('html')->put($file_name, $html);
        //Save view
        $html_view = new HtmlView();
        $html_view->path = $file_name;
        $html_view->monitoring_id = $monitoring->id;
        $html_view->created_at = date('Y-m-d H:i:s');
        $html_view->save();
        return redirect()->route('monitoring.detail', $monitoring->id)->with([
            'message-type' => 'success',
            'message-content'=>'The web monitoring has been created'
        ]);
    }

    private static function get_html_tag($html, $position, $tag){
        $it = $position;
        $end = null;
        $continue = true;
        while ($continue){
            if($html[$it] == '>') {
                $end = $it;
            }
            if(!is_null($end) && $html[$it] == '<'){
                if($html[$it + 1] == '/'){
                    $end = $it + strlen($tag) + 2;
                }
                $continue = false;
            }
            $it++;
        }
        $return['start'] = $position;
        $return['end'] = $end;
        return $return;
    }

    private static function string_search($html, $word, $is_tag = false){
        $needle = $is_tag ? '<'.$word.' ' : $word;
        $lastPos = 0;
        $positions = array();
        while (($lastPos = strpos($html, $needle, $lastPos))!== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($needle);
        }
        return $positions;
    }

    private static function atribute_value_search($html, $atribute_name, $value){
        $positions = self::string_search($html, $value);
        foreach ($positions as $index=>$it) {
            while ($it > 0){
                if( $html[$it] == '"' &&
                    substr($html, $it - strlen($atribute_name) - 1, strlen($atribute_name)) == $atribute_name &&
                    (($html[$positions[$index] + strlen($value)] == ' ') || ($html[$positions[$index] + strlen($value)] == '"'))) {
                    return true;
                }
                $it--;
            }
        }
        return false;
    }

    public static function get_content_of_elements($elements){
        $return = array();
        foreach ($elements as $element){
            $return[] = self::get_content_of_element($element);
        }
        return $return;
    }

    private static function get_content_of_element($html){
        $content = '';
        $is_content = false;
        for ($i = 0; $i < strlen($html); $i++){
            if($html[$i] == '<') {
                $is_content = false;
            }
            if($is_content) {
                $content .= $html[$i];
            }
            if($html[$i] == '>') {
                $is_content = true;
            }
        }
        return $content;
    }

    private static function new_element_filter($elements, $filter){
        $return = array();
        foreach ($elements as $element) {
            if (self::string_search($element, $filter)) {
                $return[] = $element;
            }
        }
        return $return;
    }

    private static function get_elements_by_class($elements, $class_name){
        $return = array();
        foreach ($elements as $element){
            if(self::atribute_value_search($element, 'class', $class_name)){
                $return[] = $element;
            }
        }
        return $return;
    }

    private static function get_elements_by_id($elements, $id){
        return self::new_element_filter($elements, 'id="'. $id . '"');
    }

    private static function get_elements_by_tag($html, $tag){
        $positions = self::string_search($html, $tag, true);
        $elements = array();
        foreach ($positions as $value) {
            $string = self::get_html_tag($html, $value, $tag);
            $elements[] = substr($html, $string['start'], $string['end'] - $string['start'] + 1);
        }
        return $elements;
    }

    public function select_fragment($id){
        return view('web.select_fragment', [
            'id' => $id
        ]);
    }

    public function select_condition($id, Request $request){
        $request->validate([
            'filter-tag'=>'required'
        ]);
        $tag = $request->input('filter-tag');
        $element_id = $request->input('filter-id');
        $class = $request->input('filter-class');
        $other = $request->input('filter-other');
        $elements = self::apply_filters($id, $tag, $element_id, $class, $other);
        $contents = self::get_content_of_elements($elements);
        return view('web.select_condition', [
            'monitoring_id' => $id,
            'filter_tag' => $tag,
            'filter_id' => $element_id,
            'filter_class' => $class,
            'filter_other' => $other,
            'elements'=> $elements,
            'contents'=>$contents
        ]);
    }

    public function fragment_save(Request $request){
        $request->validate([
            'monitoring_id'=>'required',
            'filter_tag'=>'required',
            'fragment_type'=>'required|in:any_change,condition'
        ]);
        $id = $request->input('monitoring_id');
        $filter = array($request->input('filter_tag'),
                        $request->input('filter_id'),
                        $request->input('filter_class'),
                        $request->input('filter_other'),
                        $request->has('content-checkbox')
                        );
        $fragment = new Fragment();
        $fragment->monitoring_id = $id;
        $fragment->fragment = serialize($filter);
        if($request->input('fragment_type') == 'condition'){
            $fragment->operator = $request->input('condition_operator');
            $fragment->value_to_compare	= $request->input('value_to_compare');
        }
        $fragment->save();
        return redirect()->route('monitoring.detail', ['id'=>$id])->with([
            'message-type' => 'success',
            'message-header'=>'Good news!',
            'message-content'=>'The fragment has been created'
        ]);
    }

}
