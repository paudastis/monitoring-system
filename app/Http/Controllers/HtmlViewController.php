<?php

namespace App\Http\Controllers;

use App\Models\HtmlView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HtmlViewController extends Controller
{

    const EXTENSION = '.html';

    public function detail($path){
        $html = Storage::disk('html')->get($path.self::EXTENSION);
        return view ('html.detail', [
            'html' => $html
        ]);
    }

    public function download($path){
        return Storage::disk('html')->download($path.self::EXTENSION);
    }

    public function delete($path){
        $html = HtmlView::where('path', $path.'.html')->first();
        $delete_instance = null;
        if(HtmlView::where('monitoring_id', $html->monitoring_id)->count() > 1){
            $delete_file = Storage::disk('html')->delete($path.self::EXTENSION);
            $delete_instance = HtmlView::where('path', $path.self::EXTENSION)->delete();
        } else {
            $delete_file = 0;
        }
        if ($delete_file && $delete_instance){
            $msg_type = 'success';
            $msg_content = 'The file has been successfully deleted';
        } else {
            $msg_type = 'error';
            $msg_content = 'Please try again later';
        }
        return redirect()->route('monitoring.detail', ['id' => $html->monitoring_id])->with([
            'message-type' => $msg_type,
            'message-content'=> $msg_content
        ]);
    }
}
