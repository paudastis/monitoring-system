<?php

namespace App\Http\Controllers;

use App\Models\Fragment;
use Illuminate\Http\Request;

class FragmentController extends Controller
{
    public function save(Request $request){
        $request->validate([
            'monitoring_id'=>'required',
            'fragment'=>'required',
            'fragment_type'=>'required|in:any_change,condition'
        ]);
        $fragment = new Fragment();
        $id = $request->input('monitoring_id');
        $fragment->monitoring_id = $id;
        $fragment->fragment = $request->input('fragment');

        if($request->input('fragment_type') == 'condition'){
            $fragment->operator = $request->input('condition_operator');
            $fragment->value_to_compare	= $request->input('value_to_compare');
        }
        $fragment->save();
        return redirect()->route('monitoring.detail', ['id'=>$id])->with([
            'message-type' => 'success',
            'message-header'=>'Good news!',
            'message-content'=>'The fragment has been created'
        ]);
    }

    public function delete($id){
        $delete = Fragment::find($id)->delete();
        if ($delete){
            $msg_type = 'success';
            $msg_content = 'The Fragment has been successfully deleted';
        } else {
            $msg_type = 'error';
            $msg_content = 'Please try again later';
        }
        return redirect()->route('monitoring.detail', ['id' => $id])->with([
            'message-type' => $msg_type,
            'message-content'=> $msg_content
        ]);
    }
}
