<?php

namespace App\Http\Controllers;

use App\Models\Api;
use App\Models\JsonResponse;
use App\Models\Monitoring;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{
    const CREATE_VIEW = 'api.create';

    public function create(){
        return view(self::CREATE_VIEW);
    }

    private function save_error($message, $id = false){
        if($id){
            return redirect()->route('api.edit', ['id'=>$id])->with([
                'message-type' => 'error',
                'message-content'=>$message
            ]);
        }
        return redirect()->route(self::CREATE_VIEW)->with([
            'message-type' => 'error',
            'message-content'=>$message
        ]);
    }

    public function save(Request $request){
        //Validate data
        $request->validate([
            'uri'=>'required'
        ]);
        //Get the header and the body and ensure the correct syntaxis
        $header_data = $this->get_header_data(  $request->has('header-checkbox'),
                                                $request->input('header-content'),
                                                $request->has('data-checkbox'),
                                                $request->input('data-content'));
        $header = $header_data[0];
        $data = $header_data[1];
        $save_error = $header_data[2];
        if($save_error){
            return $this->save_error($save_error);
        }
        //Do the api call to ensure and get the response
        $uri = $request->input('uri');
        $method = $request->input('api-method');
        $error_api_call = 'Error in the api call.';
        if($header){
            try {
                $response = Http::withHeaders(json_decode($header, true))->$method($uri, json_decode($data, true));
            } catch (\HttpException $e) {
                $save_error = true;
            } catch (\Exception $e) {
                $save_error = true;
            }
        }else {
            try {
                $response = Http::$method($uri, json_decode($data, true));
            } catch (\HttpException $e) {
                $save_error = true;
            } catch (\Exception $e) {
                $save_error = true;
            }
        }
        if($save_error){
            return $this->save_error($error_api_call);
        }
        //Save Monitoring
        $user = \Auth::user();
        $monitoring = new Monitoring();
        $monitoring->user_id = $user->id;
        $monitoring->is_active = true;
        $monitoring->type = 'Api';
        $monitoring->save();
        //Save Api
        $api = new Api();
        $api->id = $monitoring->id;
        $api->uri = $uri;
        $api->method = $method;
        $api->header = $header;
        $api->data = $data;
        $api->save();
        //Storage JSON
        $file_name = $monitoring->id.time().'.json';
        Storage::disk('json')->put($file_name, $response);
        //Save view
        $json_response = new JsonResponse();
        $json_response->path = $file_name;
        $json_response->monitoring_id = $monitoring->id;
        $json_response->created_at = date('Y-m-d H:i:s');
        $json_response->save();
        return redirect()->route('monitoring.detail', $monitoring->id)->with([
            'message-type' => 'success',
            'message-content'=>'The api monitoring has been created'
        ]);

    }

    public function edit($id){
        $edit = Api::find($id);
        return view(self::CREATE_VIEW, [
            'edit' => $edit
        ]);
    }

    private function error_json_syntax($json){
        $header_error = json_decode($json, true);
        if ($header_error === null && json_last_error() !== JSON_ERROR_NONE) {
            return true;
        }
        return false;
    }

    private function get_header_data($has_header, $header, $has_data, $data){
        $return_header = null;
        $return_data = null;
        $save_error = null;
        if($has_header){
            $return_header = $header;
            if ($this->error_json_syntax($header)) {
                $save_error = 'Header JSON syntax is not correct.';
            }
        }
        if($has_data){
            $return_data = $data;
            if ($this->error_json_syntax($data)) {
                $save_error = 'Body JSON syntax is not correct.';
            }
        }
        return array($return_header, $return_data, $save_error);
    }

    public function update(Request $request){
        $id = $request->input('monitoring_id');
        $api = Api::find($id);
        $header_data = $this->get_header_data(  $request->has('header-checkbox'),
                                                $request->input('header-content'),
                                                $request->has('data-checkbox'),
                                                $request->input('data-content'));
        $header = $header_data[0];
        $data = $header_data[1];
        $save_error = $header_data[2];
        if($save_error){
            return $this->save_error($save_error, $id);
        }
        $api->method = $request->input('api-method');
        $api->header = $header;
        $api->data = $data;
        $save = $api->save();
        if ($save) {
            $msg_type = 'success';
            $msg_content = 'The monitoring has been successfully edited';
        } else {
            $msg_type = 'error';
            $msg_content = 'Please try again later';
        }
        return redirect()->route('monitoring.detail', $api->id)->with([
            'message-type' => $msg_type,
            'message-content'=> $msg_content
        ]);
    }

    public function select_fragment($id){
        $json_response = JsonResponse::where('monitoring_id', $id)->orderBy('created_at', 'desc')->first();
        $json = Storage::disk('json')->get($json_response->path);
        $json_array = json_decode($json, true);
        return view('api.select_fragment', [
            'json' => $json_array,
            'id' => $id
        ]);
    }

    private static function get_json_value($indexs, $json, $view){
        $it = 1;
        $count = count($indexs);
        if($view) {
            $count--;
        }
        while ($it < $count){
            if(isset($json[$indexs[$it]])){
                $json = $json[$indexs[$it]];
            } else {
                break;
            }
            $it++;
        }
        return $json;
    }

    public static function get_fragment_value($fragment, $json, $view = false){
        return ApiController::get_json_value(json_decode(base64_decode($fragment)), $json, $view);
    }

    public function select_condition($id, $fragment){
        $json_response = JsonResponse::where('monitoring_id', $id)->orderBy('created_at', 'desc')->first();
        $json_file = Storage::disk('json')->get($json_response->path);
        $json = json_decode($json_file, true);
        $indexs = json_decode(base64_decode($fragment));
        $key = $indexs[count($indexs)-1];
        $value = $this->get_fragment_value($fragment, $json);
        return view('api.select_condition', [
            'id' => $id,
            'key' => $key,
            'value' => $value,
            'fragment' => $fragment
        ]);
    }
}
