<?php

namespace App\Http\Controllers;

use App\Models\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class JsonResponseController extends Controller
{

    const EXTENSION = '.json';

    public function detail($path){
        $json = Storage::disk('json')->get($path.self::EXTENSION);
        $json = json_encode(json_decode($json), JSON_PRETTY_PRINT);
        return view ('json.detail', [
            'json' => $json
        ]);
    }

    public function download($path){
        return Storage::disk('json')->download($path.self::EXTENSION);
    }

    public function delete($path){
        $json = JsonResponse::where('path', $path.'.json')->first();
        if(JsonResponse::where('monitoring_id', $json->monitoring_id)->count() > 1){
            $delete_file = Storage::disk('json')->delete($path.self::EXTENSION);
            $delete_instance = JsonResponse::where('path', $path.self::EXTENSION)->delete();
        } else {
            $delete_file = 0;
        }
        if ($delete_file && $delete_instance) {
            $msg_type = 'success';
            $msg_content = 'The file has been successfully deleted';
        } else {
            $msg_type = 'error';
            $msg_content = 'Please try again later';
        }
        return redirect()->route('monitoring.detail', ['id' => $json->monitoring_id])->with([
            'message-type' => $msg_type,
            'message-content'=> $msg_content
        ]);
    }
}
