<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function support(){
        return view('support.support');
    }

    public function send_email(Request $request){
        $request->validate([
            'email'=>'required',
            'subject'=>'required',
            'message'=>'required'
        ]);
        $content =  'From: '.$request->input('name').'<br>'.
                    'Email: '.$request->input('email').'<br>'.
                    'Subject: '.$request->input('subject').'<br>'.
                    'Content: '.$request->input('message');
        \Mail::to('monitoringsystemtfg@gmail.com')->send(new \App\Mail\SupportMail($content));
        return redirect()->route('support')->with([
            'message-type' => 'success',
            'message-content'=> 'We have received your message, soon we will send you an answer to the specified email address.'
        ]);
    }
}
