<?php

namespace App\Http\Controllers;

use App\Models\Api;
use App\Models\Fragment;
use App\Models\JsonResponse;
use App\Models\Web;
use App\Models\Monitoring;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MonitoringController extends Controller
{

    const DETAIL_VIEW = 'monitoring.detail';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $user = \Auth::user();
        $monitorings = Monitoring::where('user_id', $user->id)->orderBy('created_at', 'desc')->paginate(10);
        return view('monitoring.index', [
            'monitorings' => $monitorings
        ]);
    }

    private function detail_web($id, $web, $fragment){
        $fragment_view = null;
        if($fragment){
            $filters = unserialize($fragment->fragment);
            $fragment_view = WebController::apply_filters($id, $filters[0], $filters[1], $filters[2], $filters[3]);
            if($filters[4]){
                $fragment_view = WebController::get_content_of_elements($fragment_view);
            }
        }
        return view(self::DETAIL_VIEW, [
            'monitoring' => $web,
            'elements' => $fragment_view
        ]);
    }

    private function detail_api($api, $fragment){
        $last_object_name = null;
        $fragment_view = null;
        $last_index = null;
        $api->header ? $header =  json_encode(json_decode($api->header), JSON_PRETTY_PRINT) : $header = null;
        $api->data ? $body = json_encode(json_decode($api->data), JSON_PRETTY_PRINT) : $body = null;
        if($fragment) {
            $last_file = JsonResponse::where('monitoring_id', $api->id)->orderBy('created_at', 'desc')->first();
            if(Storage::disk('json')->exists($last_file->path)) {
                $old_response = Storage::disk('json')->get($last_file->path);
                $old_response = json_decode($old_response, true);
                $fragment_view = ApiController::get_fragment_value($fragment->fragment, $old_response, true);
                $index_array = json_decode(base64_decode($fragment->fragment));
                if(isset($index_array[count($index_array)-2])){
                    $last_object_name = $index_array[count($index_array)-2];
                }
                $last_index = $index_array[count($index_array)-1];
            }
        }
        return view(self::DETAIL_VIEW, [
            'monitoring' => $api,
            'header' => $header,
            'body' => $body,
            'fragment_view'=>$fragment_view,
            'last_index'=>$last_index,
            'last_object_name'=>$last_object_name
        ]);
    }

    public function detail($id){
        $web = Web::find($id);
        $api = Api::find($id);
        $fragment = Fragment::find($id);
        if($web){
            return $this->detail_web($id, $web, $fragment);
        }
        if($api){
            return $this->detail_api($api, $fragment);
        }
        return redirect()->route('dashboard');
    }

    public function pause($id){
        $monitoring = Monitoring::find($id);
        $monitoring->is_active = false;
        $monitoring->save();
        return redirect()->route(self::DETAIL_VIEW, $monitoring->id);

    }

    public function resume($id){
        $monitoring = Monitoring::find($id);
        $monitoring->is_active = true;
        $monitoring->save();
        return redirect()->route(self::DETAIL_VIEW, $monitoring->id);
    }

    private static function delete_files($files, $disk, $instance){
        $success = true;
        foreach ($files as $file){
            if(Storage::disk($disk)->exists($file->path)){
                $delete_file = Storage::disk($disk)->delete($file->path);
            } else {
                $delete_file = false;
            }
            $delete_instance = $instance::where('path', $file->path)->delete();
            $success = ($success && $delete_file && $delete_instance);
        }
        return $success;
    }

    public static function delete($id, $front = true){
        $monitoring = Monitoring::find($id);
        $model = 'App\Models\\'.$monitoring->type;
        $disk = ($monitoring->type == 'Web') ? 'html' : 'json';
        $instance = ($monitoring->type == 'Web') ? 'App\Models\\HtmlView' : 'App\Models\\JsonResponse';
        $subclass = $model::find($monitoring->id);
        $success = true;
        //Delete files
        $success = ($success && self::delete_files($subclass->files, $disk, $instance));
        //Delete subclass
        $success = ($success && $subclass->delete());
        //Delete fragment
        if(isset($monitoring->fragment)){
            $success = ($success && $monitoring->fragment->delete());
        }
        //Delete monitoring
        $success = ($success && $monitoring->delete());
        //Redirect with message
        if ($front){
            if ($success) {
                $msg_type = 'success';
                $msg_content = 'The monitoring has been successfully deleted';
            } else {
                $msg_type = 'error';
                $msg_content = 'Please try again later';
            }
            return redirect()->route('monitoring.index')->with([
                'message-type' => $msg_type,
                'message-content'=> $msg_content
            ]);
        }
        return $success;
    }

    public static function delete_all(){
        $monitorings = Monitoring::where('user_id', \Auth::user()->id)->get();
        $success = true;
        foreach ($monitorings as $monitoring){
            $success = ($success && self::delete($monitoring->id, false));
        }
        return $success;
    }
}
