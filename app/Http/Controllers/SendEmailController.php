<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SendEmailController extends Controller
{
    public static function notification_monitoring($monitoring_id, $email){
        $message =  "We have detected a new change in a monitoring that you are running. Our system has stored a new file with the new changes found.<br><br>".
                    "For more information, see your monitoring details page:<br><br>".
                    url('/monitoring/'.$monitoring_id);
        \Mail::to($email)->send(new \App\Mail\SendMail($message));
    }
}
