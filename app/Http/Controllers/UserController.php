<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    const DETAIL_VIEW = 'user.detail';
    const ERROR_MSG = 'Please try again later';

    public function detail(){
        return view(self::DETAIL_VIEW, [
            'user' => \Auth::user()
        ]);
    }

    public function edit(){
        return view('user.edit', [
            'user' => \Auth::user()
        ]);
    }

    public function update(Request $request){
        $request->validate([
            'name'=>'required',
            'surname'=>'required',
            'email'=>'required',
        ]);
        $user = \Auth::user();
        $email = $request->input('email');
        if($email != $user->email && User::where('email', $email)->first()){
            return redirect()->route('user.edit')->with([
                'message-type' => 'error',
                'message-content'=> 'The email address you have introduced is already used by another user.'
            ]);
        }
        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->email = $email;
        $save = $user->save();
        if ($save){
            $msg_type = 'success';
            $msg_content = 'Your personal information has been successfully updated.';
        } else {
            $msg_type = 'error';
            $msg_content = self::ERROR_MSG;
        }
        return redirect()->route(self::DETAIL_VIEW)->with([
            'message-type' => $msg_type,
            'message-content'=> $msg_content
        ]);
    }

    public function change_password(){
        return view('user.change_password');
    }

    public function update_password(Request $request){
        $request->validate([
            'password' => 'required|string|min:8',
            'password_confirm' => 'required|string|min:8',
        ]);
        $password = $request->input('password');
        $password_confirmed = $request->input('password_confirm');
        if($password != $password_confirmed){
            return redirect()->route('user.change_password')->with([
                'message-type' => 'error',
                'message-content'=> 'Passwords entered do not match.'
            ]);
        }
        $user = \Auth::user();
        $user->password = Hash::make($password);
        $save = $user->save();
        if ($save){
            $msg_type = 'success';
            $msg_content = 'Your password has been successfully updated.';
        } else {
            $msg_type = 'error';
            $msg_content = self::ERROR_MSG;
        }
        return redirect()->route(self::DETAIL_VIEW)->with([
            'message-type' => $msg_type,
            'message-content'=> $msg_content
        ]);
    }

    public function delete(){
        MonitoringController::delete_all();
        $delete = \Auth::user()->delete();
        if ($delete){
            return redirect()->route('home');
        }
        return redirect()->route(self::DETAIL_VIEW)->with([
            'message-type' => 'error',
            'message-content'=> self::ERROR_MSG
        ]);
    }

    public function disable_notifications(){
        $user = \Auth::user();
        $user->notification = false;
        $save = $user->save();
        if ($save){
            $msg_type = 'success';
            $msg_content = 'Notificactions disabled successfully.';
        } else {
            $msg_type = 'error';
            $msg_content = self::ERROR_MSG;
        }
        return redirect()->route(self::DETAIL_VIEW)->with([
            'message-type' => $msg_type,
            'message-content'=> $msg_content
        ]);
    }

    public function enable_notifications(){
        $user = \Auth::user();
        $user->notification = true;
        $save = $user->save();
        if ($save){
            $msg_type = 'success';
            $msg_content = 'Notificactions enabled successfully.';
        } else {
            $msg_type = 'error';
            $msg_content = self::ERROR_MSG;
        }
        return redirect()->route(self::DETAIL_VIEW)->with([
            'message-type' => $msg_type,
            'message-content'=> $msg_content
        ]);
    }
}
