<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Change password') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('includes.message')
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form id="filters-form" method="POST" action="{{route('user.update_password')}}">
                        @csrf
                        <div class="filter-form">
                            <x-label for="password" :value="__('New password')" />

                            <x-input id="password" class="block mt-1 w-full" type="password" name="password" :value="old('password')" required/>
                        </div>
                        <div class="filter-form">
                            <x-label for="password_confirm" :value="__('Confirm the new password')" />

                            <x-input id="password_confirm" class="block mt-1 w-full" type="password" name="password_confirm" :value="old('password_confirm')" required/>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <button id="change-password" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                {{ __('Change password') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
