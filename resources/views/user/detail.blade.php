<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Account') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('includes.message')
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="flex justify-end hidden sm:flex sm:items-center sm:ml-6">
                        <x-dropdown id="monitoring-detail-dropdown" align="right" width="48">
                            <x-slot name="trigger">
                                <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                                    <div class="ml-1">
                                        <svg class="fill-current h-8 w-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                            <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" clip-rule="evenodd" />
                                        </svg>
                                    </div>
                                </button>
                            </x-slot>
                            <x-slot name="content">
                                <x-dropdown-link href="{{ route('user.edit') }}">Edit Information</x-dropdown-link>
                                <x-dropdown-link href="{{ route('user.change_password') }}">Change password</x-dropdown-link>
                                <x-dropdown-link href="{{ $user->notification ? route('user.no_notifications') : route('user.yes_notifications') }}">
                                    {{$user->notification ? 'Disable notifications' : 'Enable notifications'}}
                                </x-dropdown-link>
                                <x-dropdown-link href="{{ route('user.delete') }}">Delete</x-dropdown-link>
                            </x-slot>
                        </x-dropdown>
                    </div>
                    <div class="clearfix"></div>
                    <p class="user-details"><b>Name:</b> {{$user->name}}</p>
                    <p class="user-details"><b>Surname:</b> {{$user->surname}}</p>
                    <p class="user-details"><b>Email:</b> {{$user->email}}</p>
                    <p class="user-details"><b>Account created at:</b> {{date("F d, Y", strtotime($user->created_at))}}</p>
                    <p><b>Email notifications:</b> {{$user->notification ? 'Yes' : 'No'}} </p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
