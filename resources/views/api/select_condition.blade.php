<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Condition fragment') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div id="select-condition" class="p-6 bg-white border-b border-gray-200">
                    <h2>Fragment selected</h2>
                    <p>
                        {{(gettype($key) == 'string') ? '"'.$key.'": ' : ''}}
                        {{(gettype($value) == 'string') ? '"'.$value.'"' : $value}}
                    </p>
                    <h2>Monitoring action for the fragment</h2>
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form id="select-condition-form" method="POST" action="{{route('fragment.save')}}">
                        @csrf
                        <input type="hidden" name="monitoring_id" value="{{$id}}">
                        <input type="hidden" name="fragment" value="{{$fragment}}">
                        <div class="condition-type-input">
                            <input type="radio" id="any_change" name="fragment_type" value="any_change" checked="checked">
                            <label for="any_change">Save any change</label>
                        </div>
                        <div class="condition-type-input">
                            <input type="radio" id="condition" name="fragment_type" value="condition">
                            <label for="condition">Save changes when</label>
                        </div>
                        <div id="condition-input">
                            {{(gettype($key) == 'string') ? '"'.$key.'"' : ''}}
                            <select name="condition_operator">
                                <option value="==">=</option>
                                <option value="!=">&#8800</option>
                                <option value="<"><</option>
                                <option value="<=">&#8804</option>
                                <option value=">">></option>
                                <option value=">=">&#8805</option>
                            </select>
                            <label for="value_to_compare"></label>
                            <input type="text" name="value_to_compare">
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <button id="start-monitoring" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
