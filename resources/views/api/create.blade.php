<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ isset($edit) ? __('Edit monitoring') : __('Start an api monitoring') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('includes.message')
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg monitoring-form">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <form id="api-form" method="POST" action="{{isset($edit) ? route('api.update'): route('api.save')}}">
                    @csrf
                    @isset($edit) <input id="edit" type="hidden" name="monitoring_id" value="{{ $edit->id }}"> @endisset
                    <select name="api-method" id="api-method" name="method">
                        <option value="get" {{ (isset($edit) and $edit->method == 'get') ? 'selected' : ''}}>GET</option>
                        <option value="post" {{ (isset($edit) and $edit->method == 'post') ? 'selected' : ''}}>POST</option>
                        <option value="put" {{ (isset($edit) and $edit->method == 'put') ? 'selected' : ''}}>PUT</option>
                        <option value="patch" {{ (isset($edit) and $edit->method == 'patch') ? 'selected' : ''}}>PATCH</option>
                        <option value="delete" {{ (isset($edit) and $edit->method == 'delete') ? 'selected' : ''}}>DELETE</option>
                    </select>
                        @if(isset($edit))
                            <div id="uri-edit">
                                <span id="uri">{!! $edit->uri !!}</span>
                            </div>
                        @else
                            <div class="uri-form">
                                <x-label for="uri" :value="__('Uri')" />

                                <x-input id="uri" class="block mt-1 w-full" type="text" name="uri" :value="old('uri')" placeholder="https://" required autofocus />
                            </div>
                        @endif
                    <div class="monitoring-actions">
                        <button type="button" id="send-api" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                            Send
                        </button>
                        <div id="load-spin-api" class="hidden-content">
                            <div class="loadingio-spinner-rolling-4kirkvc2qtx">
                                <div class="ldio-i865r5puluh">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div id="api-optional">
                        <div>
                            <span class="text-gray-700">Options</span>
                            <div class="mt-2">
                                <div>
                                    <label for="header-checkbox" class="inline-flex items-center">
                                        <input name="header-checkbox" id="header-checkbox" type="checkbox" class="form-checkbox" {!! (isset($edit) and !is_null($edit->header)) ? 'checked' : '' !!}>
                                        <span class="ml-2">Header</span>
                                    </label>
                                </div>
                                <div>
                                    <label for="data-checkbox" class="inline-flex items-center">
                                        <input name="data-checkbox" id="data-checkbox"  type="checkbox" class="form-checkbox" {!! (isset($edit) and !is_null($edit->data)) ? 'checked' : '' !!}>
                                        <span class="ml-2">Data</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <label for="header-content" id="api-header" class="block {!! (isset($edit) and !is_null($edit->header)) ? 'visible' : 'hidden-content' !!}">
                            <span class="text-gray-700">Headers</span>
                            <textarea name="header-content" id="api-header-value" class="form-textarea mt-1 block w-full border-gray-300 rounded" rows="3" placeholder="Enter some long form content.">
@if(isset($edit) and !is_null($edit->header))
    {!! $edit->header !!}
@else
{
"Content-Type": "application/json",
"Accept": "*/*"
}
@endif
                        </textarea>
                        </label>
                        <label for="data-content" id="api-body" class="block {!! (isset($edit) and !is_null($edit->data)) ? 'visible' : 'hidden-content' !!}">
                            <span class="text-gray-700">Body</span>
                            <textarea name="data-content" id="api-data-value" class="form-textarea mt-1 block w-full border-gray-300 rounded" rows="3" placeholder="Enter some long form content.">
@if(isset($edit) and !is_null($edit->data))
    {!! $edit->data !!}
@else
{

}
@endif
                            </textarea>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                    <div id="load">
                        <pre id="loaded-api" scrolling="value"></pre>
                    </div>
                    <div class="flex items-center justify-end mt-4">
                        <button id="start-monitoring" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                            {{ isset($edit) ? __('Save') :  __('Start')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
