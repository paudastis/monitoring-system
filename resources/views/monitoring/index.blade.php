<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My Monitorings') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('includes.message')
            @foreach($monitorings as $monitoring)
                @php
                    $model = 'App\Models\\'.$monitoring->type;
                    $subclass = $model::find($monitoring->id);
                @endphp
            <a href="{{ route('monitoring.detail', ['id'=>$monitoring->id]) }}">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg monitoring-element">
                    <span>{{ $subclass->url ? $subclass->url : $subclass->uri }}</span>
                    <p>created at {{date("F d, Y", strtotime($monitoring->created_at))}} | Last check {{\FormatTime::time_elapsed_string($monitoring->updated_at)}}</p>
                </div>
            </a>
            @endforeach
        </div>
        {{$monitorings->links()}}
    </div>
</x-app-layout>
