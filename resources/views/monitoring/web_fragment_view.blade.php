<div id="elements">
    @foreach($elements as $element)
        <div class="element">
            <p>
                {{$element}}
                @if(isset($monitoring->monitoring->fragment->operator))
                    <b>
                        &nbsp
                        @switch($monitoring->monitoring->fragment->operator)
                            @case('==')
                            =
                            @break
                            @case('!=')
                            &#8800
                            @break
                            @case('<=')
                            &#8804
                            @break
                            @case('>=')
                            &#8805
                            @break
                            @default
                            {{$monitoring->monitoring->fragment->operator}}
                        @endswitch
                        &nbsp
                        {{$monitoring->monitoring->fragment->value_to_compare}}
                    </b>
                @endif
            </p>
        </div>
    @endforeach
</div>
