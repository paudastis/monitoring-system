@if(gettype($fragment_view) != 'string' and array_keys($fragment_view)[0] and gettype(array_keys($fragment_view)[0]) == 'integer')
    <span>[</span>
@else
    <span>{{gettype($last_object_name) == 'string' ? $last_object_name.': ' : ''}}{</span>
@endif
<div class="clearfix"></div>

@if(gettype($fragment_view) == 'string')
    <div class="json-tab"></div>
    <span class="json-element field-selected">{{$fragment_view}}</span>
@elseif(gettype($fragment_view) == 'array')
    @foreach($fragment_view as $key => $frag)
        <div class="json-tab"></div>
        <span class="json-element {{$last_index == $key ? 'field-selected' : ''}}">
            {{(gettype($key) == 'string') ? '"'.$key.'": ' : ''}}
            {{(gettype($frag) == 'string') ? '"'.$frag.'"' : $frag}}
        </span>
        {{array_search($key,array_keys($fragment_view)) < count($fragment_view)-1 ? ',' : ''}}
        @if($last_index == $key and isset($monitoring->monitoring->fragment->operator))
            <span class="json-element">
                <b>
                    &nbsp
                    @switch($monitoring->monitoring->fragment->operator)
                        @case('==')
                            =
                            @break
                        @case('!=')
                            &#8800
                            @break
                        @case('<=')
                            &#8804
                            @break
                        @case('>=')
                            &#8805
                            @break
                        @default
                            {{$monitoring->monitoring->fragment->operator}}
                    @endswitch
                    &nbsp
                    {{$monitoring->monitoring->fragment->value_to_compare}}
                </b>
            </span>
        @endif
        <div class="clearfix"></div>
    @endforeach
@endif
<div class="clearfix"></div>

@if(gettype($fragment_view) != 'string' and array_keys($fragment_view)[0] and gettype(array_keys($fragment_view)[0]) == 'integer')
    <span>]</span>
@else <span>}</span>
@endif
<div class="clearfix"></div>
