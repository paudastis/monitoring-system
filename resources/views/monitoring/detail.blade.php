<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Monitoring details') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('includes.message')
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-14 bg-white border-b border-gray-200 monitoring-detail">
                    <div class="flex justify-end hidden sm:flex sm:items-center sm:ml-6">
                        <x-dropdown id="monitoring-detail-dropdown" align="right" width="48">
                            <x-slot name="trigger">
                                <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                                    <div class="ml-1">
                                        <svg class="fill-current h-8 w-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                            <path fill-rule="evenodd" d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" clip-rule="evenodd" />
                                        </svg>
                                    </div>
                                </button>
                            </x-slot>

                            <x-slot name="content">
                                @if(isset($monitoring->monitoring->fragment))
                                    <x-dropdown-link href="{{ route('fragment.delete', ['id' => $monitoring->id]) }}">Delete the fragment</x-dropdown-link>
                                @else
                                    <x-dropdown-link href="{{route($monitoring->url ? 'web.select_fragment' : 'api.select_fragment', ['id' => $monitoring->id])}}">Add a fragment</x-dropdown-link>
                                @endif
                                <x-dropdown-link href="{{$monitoring->monitoring->is_active ? route('monitoring.pause', ['id' => $monitoring->id]) : route('monitoring.resume', ['id' => $monitoring->id])}}">
                                    {{$monitoring->monitoring->is_active ? 'Pause' : 'Resume'}}
                                </x-dropdown-link>
                                    @isset($monitoring->uri)
                                        <x-dropdown-link href="{{ route('api.edit', ['id' => $monitoring->id]) }}">
                                            Edit
                                        </x-dropdown-link>
                                    @endisset
                                <x-dropdown-link href="{{ route('monitoring.delete', ['id' => $monitoring->id]) }}">
                                    Delete
                                </x-dropdown-link>
                            </x-slot>
                        </x-dropdown>
                    </div>
                    <div class="clearfix"></div>
                    <a href="{{$monitoring->url ? $monitoring->url : $monitoring->uri}}">{!! $monitoring->url ? '<b>Url:</b> '.$monitoring->url : '<b>Uri:</b> '.$monitoring->uri !!}</a>
                    @isset($monitoring->uri)
                        <p id="method-detail"><b>Method:</b> {{$monitoring->method}}</p>
                        <div id="api-optional-detail">
                            @isset($header)
                                <div id="header-detail">
                                    <b>Header</b>
                                    <pre class="scrollable-content-readonly" scrolling="value">{{$header}}</pre>
                                </div>
                            @endisset
                            @isset($body)
                                <div id="body-detail">
                                    <b>Body</b>
                                    <pre class="scrollable-content-readonly" scrolling="value">{{$body}}</pre>
                                </div>
                            @endisset
                        </div>
                        <div class="clearfix"></div>
                    @endisset
                    <p><b>Monitoring type:</b> {{$monitoring->monitoring->type}}</p>
                    <p><b>Alert:</b> When detects any change {{isset($monitoring->monitoring->fragment) ? 'of the fragment selected' : ''}} {{isset($monitoring->monitoring->fragment->operator) ? ' with a specific condition' : ''}}</p>
                    @isset($monitoring->monitoring->fragment)
                        <p><b>Fragment{{isset($monitoring->monitoring->fragment->operator) ? ' with the condition' : ''}}:</b></p>
                        @if($monitoring->uri)
                            @include('monitoring.api_fragment_view')
                        @else
                            @include('monitoring.web_fragment_view')
                        @endif
                    @endisset
                    <p><b>Last check:</b> {{\FormatTime::time_elapsed_string($monitoring->monitoring->updated_at)}}</p>
                    <p><b>Creation date:</b> {{date("F d, Y", strtotime($monitoring->monitoring->updated_at))}}</p>
                    <div id="active-detail">
                        <span>The monitorization is{!! $monitoring->monitoring->is_active ? ' ' : ' <b>not</b> ' !!}<b>active</b></span>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <p id="files-header"><b>History of changes:</b></p>
                    @php $files = $monitoring->files()->paginate(10); @endphp
                    <table id="history-files">
                        @foreach($files as $index=>$file)
                            <tr class="detail-file">
                                <td><span>{!! '<b>#'.(count($monitoring->files) - $index - ($files->currentpage() - 1)* 10).'</b>' . ' | ' . $file->created_at  . ' | ' . \FormatTime::time_elapsed_string($file->created_at)!!}</span></td>
                                <td><a href="{{route($monitoring->uri ? 'json.detail' : 'html.detail', ['path' => substr($file->path, 0, strlen($file->path) - 5)])}}" class="view-file" target="_blank">View</a></td>
                                <td><a href="{{route($monitoring->uri ? 'json.download' : 'html.download', ['path' => substr($file->path, 0, strlen($file->path) - 5)])}}">Download</a></td>
                                @if (count($monitoring->files) > 1)
                                    <td><a href="{{route($monitoring->uri ? 'json.delete' : 'html.delete', ['path' => substr($file->path, 0, strlen($file->path) - 5)])}}" class="delete-file">Delete</a></td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                    {{$files->links()}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
