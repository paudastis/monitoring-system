<?php
    function print_interactive_json_array($k, $arr, $depth, $parent, $id, $last, $count, $parent_array) {
        if(!is_array($arr)) {
            for ($i = 0; $i < $depth; $i++) {
                echo '<div>';
                echo '<div class="json-tab"></div>';
            }
            $parent[] = $k;
            echo '<span class="json-element">';
            echo $id ? '<a href="'.route('api.select_condition', ['id'=>$id, 'fragment'=>base64_encode(json_encode($parent))]).'">' : '';
            echo gettype($k) == 'integer' ? '' : '"'.$k.'": ';
            echo gettype($arr) == 'string' ? '"'.$arr.'"' : $arr;
            echo $id ? '</a>' : '';
            echo $last ? '' : ',';
            echo '</span>';
            echo '</div>';
            echo '<div class="clearfix"></div>';
            return;
        }else {
            $parent[] = $k;
            for ($i = 0; $i < $depth; $i++) {
                echo '<div class="json-tab"></div>';
            }
            echo '<span>';
            echo gettype($k) == 'string' ? ($k ? '"'.$k.'": ' : '') : '';
            $keys = array_keys($arr);
            $type = null;
            if(isset($keys[0])){
                $type = gettype($keys[0]);
            }
            echo $type == 'integer' ? '[' : '{';
            echo '</span>';
            echo '<div class="clearfix"></div>';
            $it = 0;
            $array_it = $k;
            foreach($arr as $k => $v) {
                $last = $it == count($arr) - 1;
                ++$it;
                print_interactive_json_array($k, $v, $depth + 1, $parent, $id, $last, count($arr) - 1, $arr);
            }
            for ($i = 0; $i < $depth; $i++) {
                echo '<div class="json-tab"></div>';
            }
            echo '<span>';
            echo $type == 'integer' ? ']' : '}';
            if(isset($parent_array)){
                echo array_search($array_it,array_keys($parent_array)) < $count ? ',' : '';
            }
            echo '</span>';
            echo '<div class="clearfix"></div>';
        }
    }

    function print_interactive_json_string($json, $id){
        $parent[] = null;
        echo '<span class="json-element">';
        echo $id ? '<a href="'.route('api.select_condition', ['id'=>$id, 'fragment'=>base64_encode(json_encode($parent))]).'">' : '';
        echo '"'.$json.'"';
        echo $id ? '</a>' : '';
        echo '</span>';
        echo '<div class="clearfix"></div>';
    }

    function print_json($json, $id = null){
        $type = gettype($json);
        if($type == 'array') {
            print_interactive_json_array(null, $json, 0, array(), $id, true, null, null);
        } else if($type == 'string'){
            print_interactive_json_string($json, $id);
        }
    }

?>
