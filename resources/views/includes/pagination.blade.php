@if ($paginator->hasPages())
    <nav id="pagination" role="navigation" aria-label="{{ __('Pagination Navigation') }}">
        <div id="pagination-pages">
            <div id="pagination-previous">
                @if ($paginator->onFirstPage())
                    <span>«</span>
                @else
                    <a href="{{ $paginator->previousPageUrl() }}">«</a>
                @endif
            </div>
            <div id="pagination-numbers">
                <span>
                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <span aria-disabled="true">
                                <span class="items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 cursor-default leading-5">{{ $element }}</span>
                            </span>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                <div class="pagination-number">
                                    @if ($page == $paginator->currentPage())
                                        <span aria-current="page">
                                            <span class="current-page">{{ $page }}</span>
                                        </span>
                                    @else
                                        <a href="{{ $url }}" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                            {{ $page }}
                                        </a>
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    @endforeach
                </span>
            </div>
            <div id="pagination-next">
                @if ($paginator->hasMorePages())
                    <a href="{{ $paginator->nextPageUrl() }}">»</a>
                @else
                    <span>»</span>
                @endif
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Showing -->
        <div id="pagination-showing">
            <p>
                {!! __('Showing') !!}
                <span class="font-medium">{{ $paginator->firstItem() }}</span>
                {!! __('to') !!}
                <span class="font-medium">{{ $paginator->lastItem() }}</span>
                {!! __('of') !!}
                <span class="font-medium">{{ $paginator->total() }}</span>
                {!! __('results') !!}
            </p>
        </div>
    </nav>
@endif
