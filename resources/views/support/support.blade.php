<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Support') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('includes.message')
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Tell us about any problems you have had with the application, suggestions or questions!
                    <br><br>
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form method="POST" action="{{ route('support.send_email') }}">
                    @csrf
                        <div>
                            <x-label for="name" :value="__('Name')" />

                            <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" />
                        </div>
                        <br>
                        <div>
                            <x-label for="email" :value="__('Email')" />

                            <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                        </div>
                        <br>
                        <div>
                            <x-label for="subject" :value="__('Subject')" />

                            <x-input id="subject" class="block mt-1 w-full" type="text" name="subject" :value="old('subject')" required />
                        </div>
                        <br>
                        <span class="lock font-medium text-sm text-gray-700">Enter message</span>
                        <textarea name="message" class="form-textarea mt-1 block w-full border-gray-300 rounded" rows="3" required></textarea>
                        <div class="flex items-center justify-end mt-4">
                            <button id="log-in" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                {{ __('Send') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
