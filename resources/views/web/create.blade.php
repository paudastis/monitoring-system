<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Start a web monitoring') }}
        </h2>
    </x-slot>

    <x-auth-validation-errors class="mb-4" :errors="$errors" />
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('includes.message')
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg monitoring-form">
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <form method="POST" action="{{route('web.save')}}">
                @csrf
                    <div class="url-form">
                        <x-label for="url" :value="__('Url')" />

                        <x-input id="url" class="block mt-1 w-full" type="text" name="url" :value="old('url')" placeholder="https://" required autofocus />
                    </div>
                    <div class="monitoring-actions">
                        <button type="button" id="load-site" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                            Load site
                        </button>
                        <button type="button" id="view-html" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                            View Html
                        </button>
                        <div id="load-spin-html" class="hidden-content">
                            <div class="loadingio-spinner-rolling-4kirkvc2qtx">
                                <div class="ldio-i865r5puluh">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div id="load">
                        <pre id="loaded-html" scrolling="value"></pre>
                        <iframe id="loaded-site" height="0px"></iframe>
                    </div>
                    <div class="flex items-center justify-end mt-4">
                        <button id="start-monitoring" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                            {{ __('Start') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
