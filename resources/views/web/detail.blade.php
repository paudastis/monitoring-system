<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Monitoring details') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{$monitoring->url}}">{{$monitoring->url}}</a>
                    <p>Monitoring type: {{$monitoring->monitoring->type}}</p>
                    <p>Alert: When detects any change</p>
                    <p>Last check: {{\FormatTime::LongTimeFilter($monitoring->monitoring->updated_at)}}</p>
                    <p>Creation date: {{date("F d, Y", strtotime($monitoring->monitoring->updated_at))}}</p>
                    @if($monitoring->monitoring->is_active)
                        <p>The monitorization is active</p>
                        <a>resume</a>
                    @else
                        <p>The monitorization is not active</p>
                        <a>pause</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
