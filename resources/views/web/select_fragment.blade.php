<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add Filters') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white border border-white-400 text-white-700 px-4 py-3 message-rounded relative overflow-hidden message-include" role="alert">
                <span class="block sm:inline">Add some filters to your monitoring to track only those elements of the web page that you are interested in.</span>
                <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                    <svg class="fill-current h-6 w-6 text-white-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
                </span>
            </div>
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form id="filters-form" method="GET" action="{{route('web.select_condition', ['id'=>$id])}}">
                        @csrf
                        <span id="monitoring_id" hidden>{{$id}}</span>
                        <div id="tag-required" class="hidden-content">
                            <span class="required">Tag is required</span>
                        </div>
                        <div class="filter-form">
                            <x-label for="filter-tag" :value="__('Tag (*)')" />

                            <x-input id="filter-tag" class="block mt-1 w-full" type="text" name="filter-tag" :value="old('filter-tag')" required autofocus />
                        </div>
                        <div class="filter-form">
                            <x-label for="filter-id" :value="__('Id')" />

                            <x-input id="filter-id" class="block mt-1 w-full" type="text" name="filter-id" :value="old('filter-id')"/>
                        </div>
                        <div class="filter-form">
                            <x-label for="filter-class" :value="__('Class')" />

                            <x-input id="filter-class" class="block mt-1 w-full" type="text" name="filter-class" :value="old('filter-class')"/>
                        </div>
                        <div class="filter-form">
                            <x-label for="filter-other" :value="__('Other')" />

                            <x-input id="filter-other" class="block mt-1 w-full" type="text" name="filter-other" :value="old('filter-other')"/>
                        </div>
                        <div class="monitoring-actions filter-action">
                            <button type="button" id="send-filters" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                                Preview
                            </button>
                            <div id="load-spin-api" class="hidden-content">
                                <div class="loadingio-spinner-rolling-4kirkvc2qtx">
                                    <div class="ldio-i865r5puluh">
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="load">
                            <pre id="loaded-elements" scrolling="value"></pre>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <button id="start-monitoring" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                {{ __('Start') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

