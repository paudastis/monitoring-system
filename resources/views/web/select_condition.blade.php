<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add a condition') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-yellow-100 border border-yellow-400 text-yellow-700 px-4 py-3 message-rounded relative overflow-hidden message-include" role="alert">
                <strong class="font-bold">Be careful!</strong>
                <span class="block sm:inline">If the fragment you select is inside an array (not inside an object) and the order of its elements is modified, it can produce errors in the monitoring.</span>
                <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                <svg class="fill-current h-6 w-6 text-yellow-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
            </span>
            </div>
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div id="select-condition" class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form id="select-condition-form" method="POST" action="{{route('web_fragment.save')}}">
                        @csrf
                        <h2>Elements selected</h2>
                        <input type="hidden" name="monitoring_id" value="{{$monitoring_id}}">
                        <input type="hidden" name="filter_tag" value="{{$filter_tag}}">
                        <input type="hidden" name="filter_id" value="{{$filter_id}}">
                        <input type="hidden" name="filter_class" value="{{$filter_class}}">
                        <input type="hidden" name="filter_other" value="{{$filter_other}}">
                        <label for="content-checkbox" id="only-content-checkbox" class="inline-flex items-center">
                            <input name="content-checkbox" id="content-checkbox" type="checkbox" class="form-checkbox">
                            <span class="ml-2">Track only the content of of each element</span>
                        </label>
                        <div id="with-content">
                            <ul class="web-elements">
                                @foreach($elements as $element)
                                    <li class="web-element">
                                        {{$element}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div id="without-content" class="hidden-content">
                            <ul class="web-elements">
                                @foreach($contents as $element)
                                    <li class="web-element">
                                        {{$element}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <h2 id="select-condition-title-web" >Monitoring action for the fragment</h2>
                        <div class="condition-type-input">
                            <input type="radio" id="any_change" name="fragment_type" value="any_change" checked="checked">
                            <label for="any_change">Save any change</label>
                        </div>
                        <div class="condition-type-input">
                            <input type="radio" id="condition" name="fragment_type" value="condition">
                            <label for="condition">Save changes when</label>
                        </div>
                        <div id="condition-input">
                            <select name="condition_operator">
                                <option value="==">=</option>
                                <option value="!=">&#8800</option>
                                <option value="<"><</option>
                                <option value="<=">&#8804</option>
                                <option value=">">></option>
                                <option value=">=">&#8805</option>
                            </select>
                            <label for="value_to_compare"></label>
                            <input type="text" name="value_to_compare">
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <button id="start-monitoring" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
