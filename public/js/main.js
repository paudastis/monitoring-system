const url=window.location.origin;
window.addEventListener("load", function(){

    function isChecked(obj){
        if(obj.is(":checked")){
            return true;
        }
        return false;
    }

    $('#view-html').click(function(){
        if($('#url').val().length > 0) {
            var button_class = $('#view-html').attr('class');
            $('#view-html').attr('class', 'hidden-content');
            $('#load-spin-html').attr('class', '');
            $.ajax({
                url: url + '/web/data?url=' + encodeURIComponent($('#url').val()),
                type: 'GET',
                success: function (response) {
                    if (response.html) {
                        document.querySelector('#loaded-html').innerText = response.html;
                    } else {
                        document.querySelector('#loaded-html').innerText = 'Error loading the site';
                    }
                },
                error: function (response) {
                    var message = JSON.stringify(JSON.parse(response.responseText), undefined, 2);
                    document.querySelector('#loaded-html').innerText = message;
                },
                complete : function(xhr, status) {
                    $('#view-html').attr('class', button_class);
                    $('#load-spin-html').attr('class', 'hidden-content');
                }
            })
        }else{
            document.querySelector('#loaded-html').innerText = null;
        }
    });

    $('#load-site').click(function(){
        if($('#url').val().length > 0) {
            $('#loaded-site').attr('src',$('#url').val());
            $('#loaded-site').attr('height', '600px');
        }else{
            $('#loaded-site').attr('height', '0px');
        }
    });

    $('#send-api').click(function(){
        uri = $('#uri').val();
        if(!uri) uri = document.getElementById("uri").innerHTML;
        if(uri.length > 0) {
            var button_class = $('#send-api').attr('class');
            $('#send-api').attr('class', 'hidden-content');
            $('#load-spin-api').attr('class', '');
            document.querySelector('#loaded-api').innerText = '';
            var header = "";
            var data = "";
            if (isChecked($('#header-checkbox'))) {
                try {
                    header = JSON.parse($('#api-header-value').val());
                } catch (error) {
                    alert('Header JSON syntax is not correct.');
                    $('#send-api').attr('class', button_class);
                    $('#load-spin-api').attr('class', 'hidden-content');
                    return;
                }
            }
            if (isChecked($('#data-checkbox'))) {
                data = $('#api-data-value').val();
                try {
                    JSON.parse(data);
                } catch (error) {
                    alert('Body JSON syntax is not correct.');
                    $('#send-api').attr('class', button_class);
                    $('#load-spin-api').attr('class', 'hidden-content');
                    return;
                }
            }
            $.ajax({
                url: uri,
                type: $('#api-method').val(),
                dataType: 'json',
                headers: header,
                data: data,
                success: function (response) {
                    if (response) {
                        document.querySelector('#loaded-api').innerText = JSON.stringify(response, undefined, 2);
                    } else {
                        document.querySelector('#loaded-api').innerText = 'null';
                    }
                },
                error: function (response) {
                    try {
                        var error = JSON.parse(response.responseText);
                        if (error['exception'] == 'Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException') {
                            alert('Error: Http not found');
                        } else {
                            var message = JSON.stringify(error, undefined, 2);
                            document.querySelector('#loaded-api').innerText = message;
                        }
                    } catch (error) {
                        alert('Error: Http not found');
                    }
                },
                complete: function (xhr, status) {
                    $('#send-api').attr('class', button_class);
                    $('#load-spin-api').attr('class', 'hidden-content');
                }
            })
        }
    });

    $('#header-checkbox').click(function (){
        if(isChecked($(this))){
            $('#api-header').attr('class', 'block visible-content');
        }
        else {
            $('#api-header').attr('class', 'block hidden-content');
        }
    });

    $('#data-checkbox').click(function (){
        if(isChecked($(this))){
            $('#api-body').attr('class', 'block visible-content');
        }
        else {
            $('#api-body').attr('class', 'block hidden-content');
        }
    });

    $(document).ready(function(){
        allowed = 'bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded';
        not_allowed = allowed + ' cursor-not-allowed';
        $('#send-api').attr('class', not_allowed);
        $('#view-html').attr('class', not_allowed);
        $('#load-site').attr('class', not_allowed);
        $('#send-filters').attr('class', not_allowed);
        edit = $('#edit').val();
        if(edit){
            $('#send-api').attr('class', allowed);
        } else {
            $('#uri').keyup(function(){
                if($(this).val().length > 0){
                    $('#send-api').attr('class', allowed);
                }
                else
                {
                    $('#send-api').attr('class', not_allowed);
                }
            })
        }

        $('#url').keyup(function(){
            if($(this).val().length > 0){
                $('#view-html').attr('class', allowed);
                $('#load-site').attr('class', allowed);
            }
            else
            {
                $('#view-html').attr('class', not_allowed);
                $('#load-site').attr('class', not_allowed);
            }
        })

        $('#filter-tag').keyup(function(){
            if($(this).val().length > 0){
                $('#send-filters').attr('class', allowed);
            }
            else
            {
                $('#send-filters').attr('class', not_allowed);
            }
        })
    });

    $('#send-filters').click(function (){
        $('#tag-required').attr('class', 'hidden-content');
        if($('#filter-tag').val().length > 0){
            var button_class = $('#send-filters').attr('class');
            $('#send-filters').attr('class', 'hidden-content')
            $('#load-spin-api').attr('class', '');
            ajax_url = url + '/monitoring/' + document.getElementById("monitoring_id").innerText + '/webFragment/search?tag=' + $('#filter-tag').val();
            if($('#filter-id').val().length > 0) ajax_url += '&id=' + $('#filter-id').val();
            if($('#filter-class').val().length > 0) ajax_url += '&class=' + $('#filter-class').val();
            if($('#filter-other').val().length > 0) ajax_url += '&other=' + $('#filter-other').val();
            $.ajax({
                url: ajax_url,
                type: 'GET',
                success: function (response) {
                    response_parsed = '';
                    if(response['elements'].length > 0){
                        for(var i = 0; i < response['elements'].length; i++) {
                            response_parsed += response['elements'][i] + '\n';
                        }
                    } else {
                        response_parsed = 'There are no elements that match with the specified conditions';
                    }
                    document.querySelector('#loaded-elements').innerText = response_parsed;
                },
                error: function (response) {
                },
                complete : function(xhr, status) {
                    $('#send-filters').attr('class', button_class);
                    $('#load-spin-api').attr('class', 'hidden-content');
                }
            })
        } else {
            $('#tag-required').attr('class', '');
        }
    });

    $('#content-checkbox').click(function (){
        if(isChecked($(this))){
            $('#without-content').attr('class', '');
            $('#with-content').attr('class', 'hidden-content');
        }
        else {
            $('#with-content').attr('class', '');
            $('#without-content').attr('class', 'hidden-content');
        }
    });

});
