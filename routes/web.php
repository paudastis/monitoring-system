<?php

use App\Http\Controllers\FragmentController;
use App\Http\Controllers\HtmlViewController;
use App\Http\Controllers\SupportController;
use App\Http\Controllers\UserController;
use App\Models\JsonResponse;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MonitoringController;
use App\Http\Controllers\WebController;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\JsonResponseController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WebController::class, 'create'])->name('home');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::prefix('web')->group(function(){
    Route::get('/create', [WebController::class, 'create'])->name('web.create');
    Route::post('/save', [WebController::class, 'save'])->middleware('auth')->name('web.save');
    Route::get('/data', [WebController::class, 'getHtml'])->name('web.getHtml');
});

Route::prefix('api')->group(function(){
    Route::get('/create', [ApiController::class, 'create'])->name('api.create');
    Route::post('/save', [ApiController::class, 'save'])->middleware('auth')->name('api.save');
});

Route::prefix('monitoring')->group(function() {
    Route::get('/', [MonitoringController::class, 'index'])
        ->name('monitoring.index')
        ->middleware('auth');
    Route::get('/{id}', [MonitoringController::class, 'detail'])
        ->name('monitoring.detail')
        ->middleware('auth')
        ->middleware('monitoring.auth');
    Route::get('/delete/{id}', [MonitoringController::class, 'delete'])
        ->name('monitoring.delete')
        ->middleware('auth')
        ->middleware('monitoring.auth');
    Route::get('api/edit/{id}', [ApiController::class, 'edit'])
        ->name('api.edit')
        ->middleware('monitoring.auth');
    Route::post('api/update', [ApiController::class, 'update'])
        ->name('api.update')
        ->middleware('auth');
    Route::get('/pause/{id}', [MonitoringController::class, 'pause'])
        ->name('monitoring.pause')
        ->middleware('monitoring.auth');
    Route::get('/resume/{id}', [MonitoringController::class, 'resume'])
        ->name('monitoring.resume')
        ->middleware('monitoring.auth');
    Route::get('/json/{path}', [JsonResponseController::class, 'detail'])
        ->name('json.detail')
        ->middleware('auth')
        ->middleware('json.auth');
    Route::get('/json/{path}/download', [JsonResponseController::class, 'download'])
        ->name('json.download')
        ->middleware('auth')
        ->middleware('json.auth');
    Route::get('/json/delete/{path}', [JsonResponseController::class, 'delete'])
        ->name('json.delete')
        ->middleware('auth')
        ->middleware('json.auth');
    Route::get('/html/{path}', [HtmlViewController::class, 'detail'])
        ->name('html.detail')
        ->middleware('auth')
        ->middleware('html.auth');
    Route::get('/html/{path}/download', [HtmlViewController::class, 'download'])
        ->name('html.download')
        ->middleware('auth')
        ->middleware('html.auth');
    Route::get('/html/delete/{path}', [HtmlViewController::class, 'delete'])
        ->name('html.delete')
        ->middleware('auth')
        ->middleware('html.auth');
    Route::get('/{id}/apiFragment/add', [ApiController::class, 'select_fragment'])
        ->name('api.select_fragment')
        ->middleware('auth')
        ->middleware('monitoring.auth')
        ->middleware('fragment.not_exists');
    Route::get('/{id}/apiFragment/add/{fragment}', [ApiController::class, 'select_condition'])
        ->name('api.select_condition')
        ->middleware('auth')
        ->middleware('monitoring.auth')
        ->middleware('fragment.not_exists');
    Route::post('/apiFragment/save', [FragmentController::class, 'save'])
        ->name('fragment.save')
        ->middleware('auth');
    Route::get('/{id}/fragment/delete', [FragmentController::class, 'delete'])
        ->name('fragment.delete')
        ->middleware('auth')
        ->middleware('monitoring.auth')
        ->middleware('fragment.exists');
    Route::get('/{id}/webFragment/add', [WebController::class, 'select_fragment'])
        ->name('web.select_fragment')
        ->middleware('monitoring.auth')
        ->middleware('fragment.not_exists');
    Route::get('/{id}/webFragment/search', [WebController::class, 'preview_get_elements'])
        ->middleware('monitoring.auth');
    Route::get('/{id}/webFragment/add/condition', [WebController::class, 'select_condition'])
        ->name('web.select_condition')
        ->middleware('monitoring.auth')
        ->middleware('fragment.not_exists');
    Route::post('/webFragment/save', [WebController::class, 'fragment_save'])
        ->name('web_fragment.save')
        ->middleware('auth');
});

Route::prefix('user')->group(function() {
    Route::get('/', [UserController::class, 'detail'])
        ->name('user.detail')
        ->middleware('auth');
    Route::get('/edit', [UserController::class, 'edit'])
        ->name('user.edit')
        ->middleware('auth');
    Route::post('/edit/update', [UserController::class, 'update'])
        ->name('user.update')
        ->middleware('auth');
    Route::get('/edit/password', [UserController::class, 'change_password'])
        ->name('user.change_password')
        ->middleware('auth');
    Route::post('/edit/password/update', [UserController::class, 'update_password'])
        ->name('user.update_password')
        ->middleware('auth');
    Route::get('/delete', [UserController::class, 'delete'])
        ->name('user.delete')
        ->middleware('auth');
    Route::get('/enableNotifications', [UserController::class, 'enable_notifications'])
        ->name('user.yes_notifications')
        ->middleware('auth');
    Route::get('/disableNotifications', [UserController::class, 'disable_notifications'])
        ->name('user.no_notifications')
        ->middleware('auth');
});

Route::get('/support', [SupportController::class, 'support'])
    ->name('support');

Route::post('/support/send', [SupportController::class, 'send_email'])
    ->name('support.send_email');

require __DIR__.'/auth.php';
