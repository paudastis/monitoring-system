<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserEditTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function test_edit_user_screen_can_be_rendered()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/user/edit');
        $response->assertStatus(200);
    }

    public function test_edit_user_information()
    {
        $user = User::factory()->create();
        $test_post = [
            'name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'email' => $this->faker->unique()->safeEmail
        ];
        $response = $this->actingAs($user)->post('/user/edit/update', $test_post);
        $response->assertRedirect('/user');
        $user_changed_info = User::find($user->id);
        $this->assertEquals($user_changed_info->name, $test_post['name']);
        $this->assertEquals($user_changed_info->surname, $test_post['surname']);
        $this->assertEquals($user_changed_info->email, $test_post['email']);
    }

    public function test_edit_user_information_with_other_user_email()
    {
        $user = User::factory()->create();
        $user_aux = User::factory()->create();
        $test_post = [
            'name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'email' => $user_aux->email
        ];
        $response = $this->actingAs($user)->post('/user/edit/update', $test_post);
        $response->assertRedirect('/user/edit');
    }

    public function test_change_user_password_screen_can_be_rendered()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/user/edit/password');
        $response->assertStatus(200);
    }

    public function test_change_user_password()
    {
        $user = User::factory()->create();
        $old_pass = $user->password;
        $test_post = [
            'password' => 'new_pass',
            'password_confirm' => 'new_pass'
        ];
        $response = $this->actingAs($user)->post('/user/edit/password/update', $test_post);
        $response->assertRedirect('/user');
        $this->assertNotEquals($old_pass, $user->password);
    }

    public function test_change_user_password_not_match()
    {
        $user = User::factory()->create();
        $new_pass = 'new_pass';
        $test_post = [
            'password' => $new_pass,
            'password_confirm' => $new_pass.'0'
        ];
        $response = $this->actingAs($user)->post('/user/edit/password/update', $test_post);
        $response->assertRedirect('/user/edit/password');
    }

    public function test_delete_user()
    {
        $user = User::factory()->create();
        $user_id = $user->id;
        $response = $this->actingAs($user)->get('/user/delete');
        $response->assertRedirect('/');
        $this->assertNull(User::find($user_id));
    }
}
