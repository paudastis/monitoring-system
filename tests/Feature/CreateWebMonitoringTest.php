<?php

namespace Tests\Feature;

use App\Models\HtmlView;
use App\Models\Monitoring;
use App\Models\Web;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateWebMonitoringTest extends TestCase
{

    use RefreshDatabase;

    public function test_create_web_monitoring_screen_can_be_rendered()
    {
        $response = $this->get('/web/create');

        $response->assertStatus(200);
    }

    public function test_save_web_monitoring_correct_url()
    {
        $user = User::factory()->create();
        $test_post = [
            'url' => 'https://example.com'
        ];
        $response = $this->actingAs($user)->post('/web/save', $test_post);
        $post = Web::where('url', $test_post['url'])->first();
        $this->assertNotNull($post);
        $response->assertRedirect('/monitoring/'.$post->id);
        $post_monitoring = Monitoring::find($post->id);
        $this->assertNotNull($post_monitoring);
        $post_html_view = HtmlView::where('monitoring_id', $post->id)->first();
        $this->assertNotNull($post_html_view);
    }

    public function test_save_web_monitoring_bad_url()
    {
        $user = User::factory()->create();
        $test_post = [
            'url' => 'example'
        ];
        $response = $this->actingAs($user)->post('/web/save', $test_post);
        $response->assertRedirect('/web/create');
    }

    public function test_save_web_monitoring_user_not_registered()
    {
        $test_post = [
            'url' => 'https://example.com'
        ];
        $response = $this->post('/web/save', $test_post);
        $response->assertRedirect('/login');
    }
}
