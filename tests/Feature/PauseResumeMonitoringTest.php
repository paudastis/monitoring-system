<?php

namespace Tests\Feature;

use App\Models\Monitoring;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PauseResumeMonitoringTest extends TestCase
{
    use RefreshDatabase;

    public function test_pause_active_monitoring()
    {
        $monitoring = Monitoring::factory()->create();
        $monitoring->is_active = true;
        $user = User::find($monitoring->user_id);
        $this->assertNotNull($user);
        $response = $this->actingAs($user)->get('/monitoring/pause/'.$monitoring->id);
        $response->assertRedirect('/monitoring/'.$monitoring->id);
        $monitoring = Monitoring::find($monitoring->id);
        $this->assertNotNull($monitoring);
        $this->assertEquals($monitoring->is_active, false);
    }

    public function test_resume_no_active_monitoring()
    {
        $monitoring = Monitoring::factory()->create();
        $monitoring->is_active = false;
        $user = User::find($monitoring->user_id);
        $this->assertNotNull($user);
        $response = $this->actingAs($user)->get('/monitoring/resume/'.$monitoring->id);
        $response->assertRedirect('/monitoring/'.$monitoring->id);
        $monitoring = Monitoring::find($monitoring->id);
        $this->assertNotNull($monitoring);
        $this->assertEquals($monitoring->is_active, true);
    }

    public function test_pause_no_active_monitoring()
    {
        $monitoring = Monitoring::factory()->create();
        $monitoring->is_active = false;
        $user = User::find($monitoring->user_id);
        $this->assertNotNull($user);
        $response = $this->actingAs($user)->get('/monitoring/pause/'.$monitoring->id);
        $response->assertRedirect('/monitoring/'.$monitoring->id);
        $monitoring = Monitoring::find($monitoring->id);
        $this->assertNotNull($monitoring);
        $this->assertEquals($monitoring->is_active, false);
    }

    public function test_resume_active_monitoring()
    {
        $monitoring = Monitoring::factory()->create();
        $monitoring->is_active = true;
        $user = User::find($monitoring->user_id);
        $this->assertNotNull($user);
        $response = $this->actingAs($user)->get('/monitoring/resume/'.$monitoring->id);
        $response->assertRedirect('/monitoring/'.$monitoring->id);
        $monitoring = Monitoring::find($monitoring->id);
        $this->assertNotNull($monitoring);
        $this->assertEquals($monitoring->is_active, true);
    }

    public function test_pause_other_user_monitoring()
    {
        $monitoring = Monitoring::factory()->create();
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/monitoring/pause/'.$monitoring->id);
        $response->assertRedirect('/dashboard');
    }

    public function test_resume_other_user_monitoring()
    {
        $monitoring = Monitoring::factory()->create();
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/monitoring/resume/'.$monitoring->id);
        $response->assertRedirect('/dashboard');
    }
}
