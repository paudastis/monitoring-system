<?php

namespace Tests\Feature;

use App\Models\JsonResponse;
use App\Models\Monitoring;
use App\Models\Api;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateApiMonitoringTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_web_monitoring_screen_can_be_rendered()
    {
        $response = $this->get('/api/create');

        $response->assertStatus(200);
    }

    public function test_save_api_monitoring_correct_uri()
    {
        $user = User::factory()->create();
        $test_post = [
            'uri' => 'https://example.com',
            'api-method' => 'get'
        ];
        $response = $this->actingAs($user)->post('/api/save', $test_post);
        $post = Api::where('uri', $test_post['uri'])->first();
        $this->assertNotNull($post);
        $response->assertRedirect('/monitoring/'.$post->id);
        $post_monitoring = Monitoring::find($post->id);
        $this->assertNotNull($post_monitoring);
        $post_json_response = JsonResponse::where('monitoring_id', $post->id)->first();
        $this->assertNotNull($post_json_response);
    }

    public function test_save_api_monitoring_bad_uri()
    {
        $user = User::factory()->create();
        $test_post = [
            'uri' => 'example',
            'api-method' => 'get'
        ];
        $response = $this->actingAs($user)->post('/api/save', $test_post);
        $response->assertRedirect('/api/create');
    }

    public function test_save_api_monitoring_user_not_registered()
    {
        $test_post = [
            'uri' => 'https://example.com'
        ];
        $response = $this->post('/api/save', $test_post);
        $response->assertRedirect('/login');
    }

    public function test_save_api_monitoring_correct_json_syntax()
    {
        $user = User::factory()->create();
        $test_post = [
            'uri' => 'https://example2.com',
            'api-method' => 'get',
            'header-checkbox' => true,
            'header-content' => '{"Content-Type": "application/json","Accept": "*/*"}'
        ];
        $response = $this->actingAs($user)->post('/api/save', $test_post);
        $post = Api::where('uri', $test_post['uri'])->first();
        $this->assertNotNull($post);
        $post_monitoring = Monitoring::find($post->id);
        $this->assertNotNull($post_monitoring);
        $post_json_response = JsonResponse::where('monitoring_id', $post->id)->first();
        $this->assertNotNull($post_json_response);
    }

    public function test_save_api_monitoring_bad_json_syntax()
    {
        $user = User::factory()->create();
        $test_post = [
            'uri' => 'https://example.com',
            'api-method' => 'get',
            'header-checkbox' => true,
            'header-content' => '{"Content-Type": "application/json" "Accept": "*/*"}'
        ];
        $response = $this->actingAs($user)->post('/api/save', $test_post);
        $response->assertRedirect('/api/create');
    }

}
