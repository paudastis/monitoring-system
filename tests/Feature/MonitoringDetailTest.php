<?php

namespace Tests\Feature;

use App\Models\HtmlView;
use App\Models\Monitoring;
use App\Models\User;
use App\Models\Web;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MonitoringDetailTest extends TestCase
{

    use RefreshDatabase;

    public function test_detail_monitoring_screen_can_be_rendered()
    {
        //Create monitoring
        $user = User::factory()->create();
        $test_post = [
            'url' => 'https://www.elperiodico.com/es/'
        ];
        $response = $this->actingAs($user)->post('/web/save', $test_post);
        $post = Web::where('url', $test_post['url'])->first();
        $this->assertNotNull($post);
        $response->assertRedirect('/monitoring/'.$post->id);
        $post_monitoring = Monitoring::find($post->id);
        $this->assertNotNull($post_monitoring);
        $post_html_view = HtmlView::where('monitoring_id', $post->id)->first();
        $this->assertNotNull($post_html_view);
        //Render
        $monitoring = Monitoring::find($post->id);
        $response = $this->actingAs($user)->get('/monitoring/'.$monitoring->id);
        $response->assertStatus(200);
    }

    public function test_detail_monitoring_of_other_user()
    {
        $monitoring = Monitoring::factory()->create();
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/monitoring/'.$monitoring->id);
        $response->assertRedirect('/dashboard');
    }
}
