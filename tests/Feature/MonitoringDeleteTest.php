<?php

namespace Tests\Feature;

use App\Models\HtmlView;
use App\Models\Monitoring;
use App\Models\User;
use App\Models\Web;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class MonitoringDeleteTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function test_delete_monitoring()
    {
        //Create monitoring
        $user = User::factory()->create();
        $test_post = [
            'url' => 'https://www.lavanguardia.com/'
        ];
        $response = $this->actingAs($user)->post('/web/save', $test_post);
        $post = Web::where('url', $test_post['url'])->first();
        $this->assertNotNull($post);
        $response->assertRedirect('/monitoring/'.$post->id);
        $post_monitoring = Monitoring::find($post->id);
        $this->assertNotNull($post_monitoring);
        $post_html_view = HtmlView::where('monitoring_id', $post->id)->first();
        $this->assertNotNull($post_html_view);
        //Delete_monitoring
        $response = $this->actingAs($user)->get('/monitoring/delete/'.$post_monitoring->id);
        $response->assertRedirect('/monitoring');
        $monitoring = Monitoring::find($post_monitoring->id);
        $this->assertNull($monitoring);
    }

    public function test_delete_other_user_monitoring()
    {
        $monitoring = Monitoring::factory()->create();
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/monitoring/delete/'.$monitoring->id);
        $response->assertRedirect('/dashboard');
    }
}
